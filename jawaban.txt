Soal 1 Membuat Database 


d:\xampp\mysql\bin>mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.4.14-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.021 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.014 sec)



Soal 2 Membuat Table di Dalam Database


MariaDB [(none)]> use myshop
Database changed
MariaDB [myshop]> create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id));
Query OK, 0 rows affected (0.056 sec)

MariaDB [myshop]> create table users(
    -> id int auto_increment,
    -> name varchar (255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id),
    -> );
Query OK, 0 rows affected (0.050 sec)

MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar (255),
    -> description varchar (255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key (category_id) references categories(id)
    -> );
Query OK, 0 rows affected (0.047 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.001 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.035 sec)

MariaDB [myshop]> describe user;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.042 sec)



MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.055 sec)



Soal 3 Memasukkan Data pada Table

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUE ("gadget");
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> INSERT INTO categories (name) VALUE ("cloth");
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> INSERT INTO categories (name) VALUE ("men");
Query OK, 1 row affected (0.009 sec)

MariaDB [myshop]> INSERT INTO categories (name) VALUE ("women");
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> INSERT INTO categories (name) VALUE ("branded");
Query OK, 1 row affected (0.009 sec)

MariaDB [myshop]> select * FROM categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
Query OK, 1 row affected (0.011 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> select *FROM items
    -> ;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

MariaDB [myshop]> INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123");
Query OK, 1 row affected (0.013 sec)

MariaDB [myshop]> INSERT INTO users (name, email, password) VALUES ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 1 row affected (0.008 sec)

MariaDB [myshop]> select *FROM users
    -> ;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)





Soal 4 Mengambil Data dari Database

a.Mengambil data users tanpa password
MariaDB [myshop]> SELECT id, name, email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)


b. Mengambil data items
MariaDB [myshop]> SELECT * FROM items WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.009 sec)

MariaDB [myshop]> SELECT * FROM items WHERE name LIKE '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.010 sec) 

c. Menampilkan data items join dengan kategori
MariaDB [myshop]> SELECT items.name, items.description, items.price, items.stock, items.category_id,
    -> categories.name FROM items JOIN categories ON
    -> items.category_id = categories.id;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.009 sec)

Soal 5 Mengubah Data dari Database
MariaDB [myshop]> update items set price=2500000 WHERE name="Sumsang b50";
Query OK, 1 row affected (0.013 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)





